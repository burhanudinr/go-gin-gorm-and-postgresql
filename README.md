# Go with Gin Gorm and Postgresql with Docker

[![pipeline status](https://gitlab.com/burhanudinr/go-gin-gorm-and-postgresql/badges/develop/pipeline.svg)](https://gitlab.com/burhanudinr/go-gin-gorm-and-postgresql/-/commits/develop)
[![coverage report](https://gitlab.com/burhanudinr/go-gin-gorm-and-postgresql/badges/develop/coverage.svg)](https://gitlab.com/burhanudinr/go-gin-gorm-and-postgresql/-/commits/develop)

## How to run with docker
1. open .env file, set DB_HOST to db_postgres
2. docker-compose up -d

### Open database GUI
1. open {host}:5050
2. create server
3. exec `docker inspect <container_database_id>` and then copy IPAddress
4. paste IPAddress in tab connection field hostname/address and set username, password

## How to run without docker
1. create DB and table manually
2. setting .env
    - PORT
    - DNS
    - DATABASE
3. go run cmd\main\main.go

## test app
### User
- [POST] {host}:{port}/user/insert
- [POST] {host}:{port}/user/login
### Item (with be with auth token from login response)
- [GET] {host}:{port}/item/get_all 
- [POST] {host}:{port}/item/insert
- [PUT] {host}:{port}/item/update
- [DELETE] {host}:{port}/item/uuid

## Postman Example (https://documenter.getpostman.com/view/12021911/TVYGcd76)

# TODO
- [x] Implement JWT
- [ ] Database Transaction
