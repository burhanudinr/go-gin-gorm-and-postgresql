package main

import (
	"fmt"
	"github.com/burhanudinr/stock-app/pkg/routes/item"
	"github.com/burhanudinr/stock-app/pkg/routes/user"
	"github.com/burhanudinr/stock-app/pkg/util"
	"github.com/gin-gonic/gin"
	"github.com/subosito/gotenv"
	"net/http"
)

func main() {
	// load .env in project
	_ = gotenv.Load()

	// set route
	handleRequest()

	fmt.Println("Stock App Start")
}

func handleRequest() {
	router := gin.Default()

	// welcome handler
	router.GET("/", welcome)

	// example
	router.GET("/example/get_all", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"data": map[string]interface{}{
				"avanza":  map[string]interface{}{
					"perjam": 50000,
					"per12jam": 250000,
					"per24jam": 450000,
				},
			},
		})
	})

	// module route
	user.Routes(router)
	item.Routes(router)

	// no route handler
	router.NoRoute(NoRoute)

	_ = router.Run(":" + util.GetEnvPort())
}

func welcome(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"success": true,
		"code":    http.StatusOK,
		"data":    "Endpoint Stock App Alive",
	})
}

func NoRoute(c *gin.Context) {
	c.JSON(http.StatusNotFound, gin.H{
		"success": false,
		"code":    http.StatusNotFound,
		"data":    "page not found!",
	})
}
