package application

import (
	"fmt"
	"github.com/burhanudinr/stock-app/pkg/util"
	"github.com/jinzhu/gorm"
	"github.com/subosito/gotenv"
)

var db *gorm.DB
var err error

func ResolveDB() *gorm.DB {
	return openDb()
}

// Open Connection
func openDb() *gorm.DB {
	_ = gotenv.Load()

	driver := util.GetEnvDriver()
	host := util.GetEnvHostDB()
	port := util.GetEnvPortDB()
	user := util.GetEnvUserDB()
	password := util.GetEnvPassDB()
	name := util.GetEnvNameDB()
	ssl := util.GetEnvSsl()

	dsn := fmt.Sprintf("host=%s port=%s user=%s dbname=%s sslmode=%s password=%s", host, port, user, name, ssl, password)
	if db, err = gorm.Open(driver, dsn); err != nil {
		panic(err)
	}

	return db
}

// close connection
func closeDb() error {
	return db.Close()
}
