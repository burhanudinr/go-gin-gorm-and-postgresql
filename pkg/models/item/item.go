package item

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

type Item struct {
	gorm.Model
	Uuid string `json:"uuid" binding:"required"`
	Code string `json:"code" validate:"required"`
	Name string `json:"name" validate:"required"`
}

func (i *Item) TableName() string {
	return "item"
}
