package user

import (
	"errors"
	"github.com/burhanudinr/stock-app/application"
	m "github.com/burhanudinr/stock-app/pkg/models/user"
	"github.com/burhanudinr/stock-app/pkg/repositories/user"
	"github.com/burhanudinr/stock-app/pkg/util"
	"github.com/thanhpk/randstr"
	"golang.org/x/crypto/bcrypt"
)

type Service interface {
	Insert(user m.User) (m.User, error)
	Login(username string, password string) (m.User, error)
	Delete(uuid string) (bool, error)
}

type userService struct {
	userRepository user.Repository
}

func GetService() *userService {
	return &userService{}
}

func (s *userService) getUserRepository() user.Repository {
	if s.userRepository != nil {
		return s.userRepository
	}

	return user.SetRepository(application.ResolveDB())
}

func (s *userService) Insert(u m.User) (m.User, error) {
	if u.Username == "" || u.Password == "" {
		return m.User{}, errors.New("username / password" + util.EmptyVar)
	}

	salt := randstr.String(10)
	pass := u.Password + salt

	encryptPass, err := bcrypt.GenerateFromPassword([]byte(pass), 14)
	if err != nil {
		return m.User{}, err
	}

	u.Password = string(encryptPass)
	u.Salt = salt
	return s.getUserRepository().Insert(u)
}

func (s *userService) Login(username string, password string) (m.User, error) {
	if username == "" || password == "" {
		return m.User{}, errors.New("username / password" + util.EmptyVar)
	}

	u, err := s.getUserRepository().GetByUsername(username)
	if err != nil {
		return m.User{}, errors.New("username not found!")
	}

	err = bcrypt.CompareHashAndPassword([]byte(u.Password), []byte(password+u.Salt))
	if err != nil {
		return m.User{}, errors.New("password not match!")
	}

	return u, nil
}

func (s *userService) Delete(uuid string) (bool, error) {
	if uuid == "" {
		return false, errors.New("uuid" + util.EmptyVar)
	}

	return s.getUserRepository().Delete(uuid)
}
