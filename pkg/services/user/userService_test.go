package user

import (
	"errors"
	model "github.com/burhanudinr/stock-app/pkg/models/user"
	"github.com/burhanudinr/stock-app/pkg/util"
	"github.com/go-playground/assert/v2"
	"github.com/jinzhu/gorm"
	"github.com/stretchr/testify/mock"
	"testing"
	"time"
)

type MockRepository struct {
	mock.Mock
}

func (mockRepo *MockRepository) Delete(uuid string) (bool, error) {
	args := mockRepo.Called(uuid)
	return args.Bool(0), args.Error(1)
}

func (mockRepo *MockRepository) GetAll() ([]model.User, error) {
	args := mockRepo.Called()
	return args.Get(0).([]model.User), args.Error(1)
}

func (mockRepo *MockRepository) Insert(user model.User) (model.User, error) {
	args := mockRepo.Called(user)
	return args.Get(0).(model.User), args.Error(1)
}

func (mockRepo *MockRepository) GetByUuid(uuid string) (model.User, error) {
	args := mockRepo.Called(uuid)
	return args.Get(0).(model.User), args.Error(1)
}

func (mockRepo *MockRepository) Update(user model.User) (model.User, error) {
	args := mockRepo.Called(user)
	return args.Get(0).(model.User), args.Error(1)
}

func TestUserService_GetAll(t *testing.T) {
	data1 := model.User{Uuid: "uuid-1"}
	data2 := model.User{Uuid: "uuid-2"}

	mockRepo := new(MockRepository)
	mockRepo.On("GetAll").Return([]model.User{data1, data2}, nil)

	testService := userService{userRepository: mockRepo}
	res, err := testService.GetAll()

	mockRepo.AssertExpectations(t)

	assert.Equal(t, res[0], data1)
	assert.Equal(t, res[1], data2)
	assert.Equal(t, err, nil)
}

func TestUserService_GetByUuid(t *testing.T) {
	data := model.User{Uuid: "test-uuid", Name: "test-name", Code: "test-code"}

	mockRepo := new(MockRepository)
	mockRepo.On("GetByUuid", data.Uuid).Return(data, nil)

	testService := userService{userRepository: mockRepo}
	res, err := testService.GetByUuid(data.Uuid)

	mockRepo.AssertExpectations(t)

	assert.Equal(t, res, data)
	assert.Equal(t, err, nil)
}

func TestUserService_GetByUuid_Fail(t *testing.T) {
	testService := userService{}
	res, err := testService.GetByUuid("")

	assert.Equal(t, res, model.User{})
	assert.Equal(t, err, errors.New("uuid"+util.EmptyVar))
}

func TestUserService_Insert(t *testing.T) {
	testData := model.User{Name: "test-name", Code: "test-code"}
	returnData := model.User{
		Model: gorm.Model{
			ID:        1,
			CreatedAt: time.Time{},
			UpdatedAt: time.Time{},
			DeletedAt: nil,
		},
		Uuid: "test-uuid",
		Code: "test-code",
		Name: "test-name",
	}

	mockRepo := new(MockRepository)
	mockRepo.On("Insert", testData).Return(returnData, nil)

	testService := userService{userRepository: mockRepo}
	res, err := testService.Insert(testData)

	mockRepo.AssertExpectations(t)

	assert.Equal(t, res, returnData)
	assert.Equal(t, err, nil)
}

func TestUserService_Update(t *testing.T) {
	testData := model.User{Uuid: "test-uuid", Name: "test-name", Code: "test-code"}
	returnData := model.User{
		Model: gorm.Model{
			ID:        1,
			CreatedAt: time.Time{},
			UpdatedAt: time.Time{},
			DeletedAt: nil,
		},
		Uuid: "test-uuid",
		Code: "test-code",
		Name: "test-name",
	}

	mockRepo := new(MockRepository)
	mockRepo.On("Update", testData).Return(returnData, nil)

	testService := userService{userRepository: mockRepo}
	res, err := testService.Update(testData)

	mockRepo.AssertExpectations(t)

	assert.Equal(t, res, returnData)
	assert.Equal(t, err, nil)
}

func TestUserService_Update_Fail(t *testing.T) {
	testService := userService{}
	res, err := testService.Update(model.User{})

	assert.Equal(t, res, model.User{})
	assert.Equal(t, err, errors.New("uuid"+util.EmptyVar))
}

func TestUserService_Delete(t *testing.T) {
	mockRepo := new(MockRepository)
	mockRepo.On("Delete", "test-uuid").Return(true, nil)

	testService := userService{userRepository: mockRepo}
	resResult, err := testService.Delete("test-uuid")

	mockRepo.AssertExpectations(t)

	assert.Equal(t, err, nil)
	assert.Equal(t, resResult, true)
}

func TestUserService_Delete_Fail(t *testing.T) {
	testService := userService{}
	res, resErr := testService.Delete("")

	assert.Equal(t, res, false)
	assert.Equal(t, resErr, errors.New("uuid"+util.EmptyVar))
}
