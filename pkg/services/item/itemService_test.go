package item

import (
	"errors"
	model "github.com/burhanudinr/stock-app/pkg/models/item"
	"github.com/burhanudinr/stock-app/pkg/util"
	"github.com/go-playground/assert/v2"
	"github.com/jinzhu/gorm"
	"github.com/stretchr/testify/mock"
	"testing"
	"time"
)

type MockRepository struct {
	mock.Mock
}

func (mockRepo *MockRepository) Delete(uuid string) (bool, error) {
	args := mockRepo.Called(uuid)
	return args.Bool(0), args.Error(1)
}

func (mockRepo *MockRepository) GetAll() ([]model.Item, error) {
	args := mockRepo.Called()
	return args.Get(0).([]model.Item), args.Error(1)
}

func (mockRepo *MockRepository) Insert(item model.Item) (model.Item, error) {
	args := mockRepo.Called(item)
	return args.Get(0).(model.Item), args.Error(1)
}

func (mockRepo *MockRepository) GetByUuid(uuid string) (model.Item, error) {
	args := mockRepo.Called(uuid)
	return args.Get(0).(model.Item), args.Error(1)
}

func (mockRepo *MockRepository) Update(item model.Item) (model.Item, error) {
	args := mockRepo.Called(item)
	return args.Get(0).(model.Item), args.Error(1)
}

func TestItemService_GetAll(t *testing.T) {
	data1 := model.Item{Uuid: "uuid-1"}
	data2 := model.Item{Uuid: "uuid-2"}

	mockRepo := new(MockRepository)
	mockRepo.On("GetAll").Return([]model.Item{data1, data2}, nil)

	testService := itemService{itemRepository: mockRepo}
	res, err := testService.GetAll()

	mockRepo.AssertExpectations(t)

	assert.Equal(t, res[0], data1)
	assert.Equal(t, res[1], data2)
	assert.Equal(t, err, nil)
}

func TestItemService_GetByUuid(t *testing.T) {
	data := model.Item{Uuid: "test-uuid", Name: "test-name", Code: "test-code"}

	mockRepo := new(MockRepository)
	mockRepo.On("GetByUuid", data.Uuid).Return(data, nil)

	testService := itemService{itemRepository: mockRepo}
	res, err := testService.GetByUuid(data.Uuid)

	mockRepo.AssertExpectations(t)

	assert.Equal(t, res, data)
	assert.Equal(t, err, nil)
}

func TestItemService_GetByUuid_Fail(t *testing.T) {
	testService := itemService{}
	res, err := testService.GetByUuid("")

	assert.Equal(t, res, model.Item{})
	assert.Equal(t, err, errors.New("uuid"+util.EmptyVar))
}

func TestItemService_Insert(t *testing.T) {
	testData := model.Item{Name: "test-name", Code: "test-code"}
	returnData := model.Item{
		Model: gorm.Model{
			ID:        1,
			CreatedAt: time.Time{},
			UpdatedAt: time.Time{},
			DeletedAt: nil,
		},
		Uuid: "test-uuid",
		Code: "test-code",
		Name: "test-name",
	}

	mockRepo := new(MockRepository)
	mockRepo.On("Insert", testData).Return(returnData, nil)

	testService := itemService{itemRepository: mockRepo}
	res, err := testService.Insert(testData)

	mockRepo.AssertExpectations(t)

	assert.Equal(t, res, returnData)
	assert.Equal(t, err, nil)
}

func TestItemService_Insert_Fail(t *testing.T) {
	testData := model.Item{}
	testService := itemService{}
	res, err := testService.Insert(testData)

	assert.Equal(t, res, model.Item{
		Model: gorm.Model{},
		Uuid:  "",
		Code:  "",
		Name:  "",
	})
	assert.Equal(t, err.Error(), "Field Item.Code is required. Field Item.Name is required. ")
}

func TestItemService_Update(t *testing.T) {
	testData := model.Item{Uuid: "test-uuid", Name: "test-name", Code: "test-code"}
	returnData := model.Item{
		Model: gorm.Model{
			ID:        1,
			CreatedAt: time.Time{},
			UpdatedAt: time.Time{},
			DeletedAt: nil,
		},
		Uuid: "test-uuid",
		Code: "test-code",
		Name: "test-name",
	}

	mockRepo := new(MockRepository)
	mockRepo.On("Update", testData).Return(returnData, nil)

	testService := itemService{itemRepository: mockRepo}
	res, err := testService.Update(testData)

	mockRepo.AssertExpectations(t)

	assert.Equal(t, res, returnData)
	assert.Equal(t, err, nil)
}

func TestItemService_Update_Fail(t *testing.T) {
	testService := itemService{}
	res, err := testService.Update(model.Item{})

	assert.Equal(t, res, model.Item{})
	assert.Equal(t, err, errors.New("uuid"+util.EmptyVar))
}

func TestItemService_Delete(t *testing.T) {
	mockRepo := new(MockRepository)
	mockRepo.On("Delete", "test-uuid").Return(true, nil)

	testService := itemService{itemRepository: mockRepo}
	resResult, err := testService.Delete("test-uuid")

	mockRepo.AssertExpectations(t)

	assert.Equal(t, err, nil)
	assert.Equal(t, resResult, true)
}

func TestItemService_Delete_Fail(t *testing.T) {
	testService := itemService{}
	res, resErr := testService.Delete("")

	assert.Equal(t, res, false)
	assert.Equal(t, resErr, errors.New("uuid"+util.EmptyVar))
}
