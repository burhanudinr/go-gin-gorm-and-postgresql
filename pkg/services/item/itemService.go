package item

import (
	"errors"
	"github.com/burhanudinr/stock-app/application"
	m "github.com/burhanudinr/stock-app/pkg/models/item"
	"github.com/burhanudinr/stock-app/pkg/repositories/item"
	"github.com/burhanudinr/stock-app/pkg/util"
	"github.com/go-playground/validator/v10"
)

type Service interface {
	GetAll() ([]m.Item, error)
	Insert(item m.Item) (m.Item, error)
	GetByUuid(uuid string) (m.Item, error)
	Update(item m.Item) (m.Item, error)
	Delete(uuid string) (bool, error)
}

type itemService struct {
	itemRepository item.Repository
}

func GetService() *itemService {
	return &itemService{}
}

func (s *itemService) getItemRepository() item.Repository {
	if s.itemRepository != nil {
		return s.itemRepository
	}

	return item.SetRepository(application.ResolveDB())
}

func (s *itemService) GetAll() ([]m.Item, error) {
	return s.getItemRepository().GetAll()
}

func (s *itemService) Insert(i m.Item) (m.Item, error) {
	if err := validateItem(i); err != nil {
		return m.Item{}, err
	}
	return s.getItemRepository().Insert(i)
}

func (s *itemService) Update(i m.Item) (m.Item, error) {
	if i.Uuid == "" {
		return m.Item{}, errors.New("uuid" + util.EmptyVar)
	}

	return s.getItemRepository().Update(i)
}

func (s *itemService) GetByUuid(uuid string) (m.Item, error) {
	if uuid == "" {
		return m.Item{}, errors.New("uuid" + util.EmptyVar)
	}

	return s.getItemRepository().GetByUuid(uuid)
}

func (s *itemService) Delete(uuid string) (bool, error) {
	if uuid == "" {
		return false, errors.New("uuid" + util.EmptyVar)
	}

	return s.getItemRepository().Delete(uuid)
}

func validateItem(i m.Item) error {
	validate := validator.New()
	err := validate.Struct(i)
	if err != nil {
		var errMsg string
		for _, err := range err.(validator.ValidationErrors) {
			errMsg = errMsg + "Field " + err.Namespace() + " is " + err.ActualTag() + ". "
		}
		return errors.New(errMsg)
	}

	return nil
}
