package user

import (
	"errors"
	model "github.com/burhanudinr/stock-app/pkg/models/user"
	"github.com/burhanudinr/stock-app/pkg/services/user"
	"github.com/burhanudinr/stock-app/pkg/util"
	"github.com/gin-gonic/gin"
	"net/http"
)

var err error

func Routes(route *gin.Engine) {
	routerUser := route.Group("/user")
	{
		routerUser.POST("/insert", Insert)
		routerUser.POST("/login", Login)
		routerUser.DELETE("/:uuid", Delete)
	}
}

func Insert(c *gin.Context) {
	var u model.User
	if err := c.ShouldBindJSON(&u); err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
			"code":    http.StatusInternalServerError,
			"success": false,
			"data":    util.FailedToDecode,
		})
		return
	}

	res, err := user.GetService().Insert(u)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
			"code":    http.StatusInternalServerError,
			"success": false,
			"data":    err.Error(),
		})
	} else {
		token := util.CreateToken(res.Uuid)

		c.JSON(http.StatusOK, gin.H{
			"code":    http.StatusOK,
			"success": true,
			"data":    res,
			"token":   token,
		})
	}
}

func Login(c *gin.Context) {
	var u model.User
	if err := c.ShouldBindJSON(&u); err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
			"code":    http.StatusInternalServerError,
			"success": false,
			"data":    util.FailedToDecode,
		})
		return
	}

	res, err := user.GetService().Login(u.Username, u.Password)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
			"code":    http.StatusInternalServerError,
			"success": false,
			"data":    err.Error(),
		})
	} else {
		token := util.CreateToken(res.Uuid)
		c.JSON(http.StatusOK, gin.H{
			"code":    http.StatusOK,
			"success": true,
			"data":    res,
			"token":   token,
		})
	}
}

func Delete(c *gin.Context) {
	success := false

	uuid, isFound := c.Params.Get("uuid")
	if isFound {
		success, err = user.GetService().Delete(uuid)
	} else {
		err = errors.New("uuid" + util.EmptyVar)
	}

	if success {
		c.JSON(http.StatusOK, gin.H{
			"code":    http.StatusOK,
			"success": success,
			"data":    true,
		})
	} else {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
			"code":    http.StatusInternalServerError,
			"success": success,
			"data":    err.Error(),
		})
	}
}
