package item

import (
	"errors"
	"fmt"
	model "github.com/burhanudinr/stock-app/pkg/models/item"
	"github.com/burhanudinr/stock-app/pkg/services/item"
	"github.com/burhanudinr/stock-app/pkg/util"
	"github.com/gin-gonic/gin"
	"net/http"
)

var err error

func Routes(r *gin.Engine) {
	mw, err := util.MiddlewareInit()
	if err != nil {
		fmt.Println(err)
	}

	rg := r.Group("/item")
	rg.Use(mw)
	{
		rg.GET("/get_all", GetAll)
		rg.POST("/insert", Insert)
		rg.PUT("/update", Update)
		rg.DELETE("/:uuid", Delete)
	}
}

func GetAll(c *gin.Context) {
	// if you want extract claim
	// claims := jwt.ExtractClaims(c)
	// claims["userUuid"]

	items, err := item.GetService().GetAll()
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
			"code":    http.StatusInternalServerError,
			"success": false,
			"data":    err,
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"code":    http.StatusOK,
		"success": true,
		"data":    items,
	})
}

func Insert(c *gin.Context) {
	var i model.Item
	if err := c.ShouldBindJSON(&i); err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
			"code":    http.StatusInternalServerError,
			"success": false,
			"data":    util.FailedToDecode,
		})
		return
	}

	res, err := item.GetService().Insert(i)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
			"code":    http.StatusInternalServerError,
			"success": false,
			"data":    err.Error(),
		})
	} else {
		c.JSON(http.StatusOK, gin.H{
			"code":    http.StatusOK,
			"success": true,
			"data":    res,
		})
	}
}

func Update(c *gin.Context) {
	var i model.Item
	if err := c.ShouldBindJSON(&i); err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
			"code":    http.StatusInternalServerError,
			"success": false,
			"data":    util.FailedToDecode,
		})
		return
	}

	res, err := item.GetService().Update(i)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
			"code":    http.StatusInternalServerError,
			"success": false,
			"data":    err.Error(),
		})
	} else {
		c.JSON(http.StatusOK, gin.H{
			"code":    http.StatusOK,
			"success": true,
			"data":    res,
		})
	}
}

func Delete(c *gin.Context) {
	success := false

	uuid, isFound := c.Params.Get("uuid")
	if isFound {
		success, err = item.GetService().Delete(uuid)
	} else {
		err = errors.New("uuid" + util.EmptyVar)
	}

	if success {
		c.JSON(http.StatusOK, gin.H{
			"code":    http.StatusOK,
			"success": success,
			"data":    true,
		})
	} else {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
			"code":    http.StatusInternalServerError,
			"success": success,
			"data":    err.Error(),
		})
	}
}
