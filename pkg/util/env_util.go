package util

import "os"

func getEnv(key, devEnv string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return devEnv
}

// database
func GetEnvDriver() string {
	return getEnv("DB_DRIVER", "postgres")
}
func GetEnvPortDB() string {
	return getEnv("DB_PORT", "5432")
}
func GetEnvHostDB() string {
	return getEnv("DB_HOST", "localhost")
}
func GetEnvUserDB() string {
	return getEnv("DB_USER", "postgres")
}
func GetEnvPassDB() string {
	return getEnv("DB_PASS", "postgres")
}
func GetEnvNameDB() string {
	return getEnv("DB_NAME", "demo")
}
func GetEnvSsl() string {
	return getEnv("DB_SSL", "disable")
}

// port
func GetEnvPort() string {
	return getEnv("PORT", "8080")
}

// JWT
func GetEnvJwtSecret() string {
	return getEnv("JWT_SECRET", "val_secret")
}