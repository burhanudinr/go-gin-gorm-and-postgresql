package util

const (
	FailedToDecode = "failed to decode!"
	EmptyVar       = " must be set!"
	NotFound       = " not found!"
)
