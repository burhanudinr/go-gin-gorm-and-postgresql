package util

import (
	"fmt"
	jwt "github.com/appleboy/gin-jwt/v2"
	m "github.com/burhanudinr/stock-app/pkg/models/user"
	"github.com/gin-gonic/gin"
	"time"
)

func MiddlewareSetUp() (*jwt.GinJWTMiddleware, error) {
	identityKey := "userUuid"

	return jwt.New(&jwt.GinJWTMiddleware{
		Realm:            "test zone",
		SigningAlgorithm: "HS512",
		Key:              []byte(GetEnvJwtSecret()),
		Timeout:          time.Hour * 9999,
		MaxRefresh:       time.Hour,
		IdentityKey:      identityKey,
		PayloadFunc: func(data interface{}) jwt.MapClaims {
			if v, ok := data.(*m.User); ok && v.Uuid != "" {
				return jwt.MapClaims{
					identityKey: v.Uuid,
				}
			}
			return jwt.MapClaims{}
		},
		IdentityHandler: func(c *gin.Context) interface{} {
			claims := jwt.ExtractClaims(c)
			return &m.User{
				Uuid: claims[identityKey].(string),
			}
		},
		Authorizator: func(data interface{}, c *gin.Context) bool {
			if _, ok := data.(*m.User); ok {
				return true
			}

			return false
		},
		Unauthorized: func(c *gin.Context, code int, message string) {
			c.JSON(code, gin.H{
				"code":    code,
				"message": message,
			})
		},
		// TokenLookup is a string in the form of "<source>:<name>" that is used
		// to extract token from the request.
		// Optional. Default value "header:Authorization".
		// Possible values:
		// - "header:<name>"
		// - "query:<name>"
		// - "cookie:<name>"
		// - "param:<name>"
		TokenLookup: "header: Authorization, query: token, cookie: jwt",
		// TokenLookup: "query:token",
		// TokenLookup: "cookie:token",

		// TokenHeadName is a string in the header. Default value is "Bearer"
		TokenHeadName: "Bearer",

		// TimeFunc provides the current time. You can override it to use another time value. This is useful for testing or if your server uses a different time zone than your tokens.
		TimeFunc: time.Now,
	})
}

func MiddlewareInit() (gin.HandlerFunc, error) {
	authMiddleware, err := MiddlewareSetUp()

	if err != nil {
		return nil, err
	}

	err = authMiddleware.MiddlewareInit()
	if err != nil {
		return nil, err
	}

	return authMiddleware.MiddlewareFunc(), nil
}

func CreateToken(userUuid string) string {
	authMiddleware, err := MiddlewareSetUp()
	if err != nil {
		fmt.Print(err)
	}

	token, _, err := authMiddleware.TokenGenerator(&m.User{Uuid: userUuid})
	if err != nil {
		fmt.Print(err)
	}
	return token
}
