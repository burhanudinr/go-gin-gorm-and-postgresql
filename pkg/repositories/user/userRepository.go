package user

import (
	"errors"
	model "github.com/burhanudinr/stock-app/pkg/models/user"
	"github.com/burhanudinr/stock-app/pkg/util"
	"github.com/google/uuid"
	"github.com/jinzhu/gorm"
)

type repo struct {
	db *gorm.DB
}

type Repository interface {
	Insert(user model.User) (model.User, error)
	GetByUsername(username string) (model.User, error)
	Delete(uuid string) (bool, error)
}

func SetRepository(db *gorm.DB) Repository {
	return &repo{
		db: db,
	}
}
func (p *repo) Insert(user model.User) (model.User, error) {
	user.Uuid = uuid.New().String()

	p.db.Create(&user)
	return user, nil
}

func (p *repo) GetByUsername(username string) (model.User, error) {
	user := model.User{}
	err := p.db.Where("username = ?", username).Find(&user).Error

	return user, err
}

func (p *repo) Delete(uuid string) (bool, error) {
	// get user by uuid
	var user model.User
	if p.db.Where("uuid = ?", uuid).Find(&user).RecordNotFound() {
		return false, errors.New(p.db.NewScope(&user).TableName() + util.NotFound)
	}

	p.db.Model(&user).Delete(user)

	return true, nil
}
