package user

import (
	"database/sql"
	"errors"
	"github.com/DATA-DOG/go-sqlmock"
	model "github.com/burhanudinr/stock-app/pkg/models/user"
	"github.com/burhanudinr/stock-app/pkg/util"
	"github.com/go-test/deep"
	"github.com/jinzhu/gorm"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	"regexp"
	"testing"
	"time"
)

type Suite struct {
	suite.Suite
	DB   *gorm.DB
	mock sqlmock.Sqlmock

	repository Repository
	User       *model.User
}

func (s *Suite) SetupSuite() {
	var (
		db  *sql.DB
		err error
	)

	db, s.mock, err = sqlmock.New()
	require.NoError(s.T(), err)

	s.DB, err = gorm.Open("postgres", db)
	require.NoError(s.T(), err)

	s.DB.LogMode(true)

	s.repository = SetRepository(s.DB)
}

func (s *Suite) AfterTest(_, _ string) {
	require.NoError(s.T(), s.mock.ExpectationsWereMet())
}

func TestInit(t *testing.T) {
	suite.Run(t, new(Suite))
}

func (s *Suite) TestUserRepository_GetByUsername() {
	var (
		id       = uint(1)
		uuid     = "test-uuid"
		username = "test-username"
		password = "test-password"
		salt     = "test-salt"
		timeNow  = time.Now()
	)

	s.mock.ExpectQuery(regexp.QuoteMeta(
		`SELECT * FROM "user" WHERE "user"."deleted_at" IS NULL AND ((username = $1))`)).
		WithArgs(username).
		WillReturnRows(sqlmock.NewRows([]string{"id", "uuid", "username", "password", "salt", "created_at", "updated_at", "deleted_at"}).
			AddRow(id, uuid, username, password, salt, timeNow, timeNow, nil))

	res, err := s.repository.GetByUsername(username)

	require.NoError(s.T(), err)
	require.Nil(s.T(), deep.Equal(model.User{
		Model: gorm.Model{
			ID:        id,
			CreatedAt: timeNow,
			UpdatedAt: timeNow,
			DeletedAt: nil,
		},
		Uuid:     uuid,
		Username: username,
		Password: password,
		Salt:     salt,
	}, res))
}

func (s *Suite) TestUserRepository_Insert() {
	var (
		id       = uint(1)
		username = "test-username"
		password = "test-password"
		salt     = "test-salt"
	)

	s.mock.ExpectBegin()
	s.mock.ExpectQuery(regexp.QuoteMeta(
		`INSERT INTO "user" ("created_at","updated_at","deleted_at","uuid","username","password","salt") VALUES ($1,$2,$3,$4,$5,$6,$7) RETURNING "user"."id"`)).
		WithArgs(sqlmock.AnyArg(), sqlmock.AnyArg(), nil, sqlmock.AnyArg(), username, password, salt).
		WillReturnRows(
			sqlmock.NewRows([]string{"id"}).
				AddRow(id))
	s.mock.ExpectCommit()

	res, err := s.repository.Insert(model.User{
		Username: username,
		Password: password,
		Salt:     salt,
	})

	require.NoError(s.T(), err)
	require.Nil(s.T(), deep.Equal(model.User{
		Model: gorm.Model{
			ID:        id,
			CreatedAt: res.CreatedAt,
			UpdatedAt: res.UpdatedAt,
			DeletedAt: nil,
		},
		Uuid:     res.Uuid,
		Username: username,
		Password: password,
		Salt:     salt,
	}, res))
}

func (s *Suite) TestUserRepository_Delete() {
	var (
		id       = uint(1)
		uuid     = "test-uuid"
		username = "test-username"
		password = "test-password"
		salt     = "test-salt"
		timeNow  = time.Now()
	)

	// query get user by uuid
	s.mock.ExpectQuery(regexp.QuoteMeta(
		`SELECT * FROM "user" WHERE "user"."deleted_at" IS NULL AND ((uuid = $1))`)).
		WithArgs(uuid).
		WillReturnRows(sqlmock.NewRows([]string{"id", "uuid", "username", "password", "salt", "created_at", "updated_at", "deleted_at"}).
			AddRow(id, uuid, username, password, salt, timeNow, timeNow, nil))

	// query delete
	s.mock.ExpectBegin()
	s.mock.ExpectExec(regexp.QuoteMeta(
		`UPDATE "user" SET "deleted_at"=$1 WHERE "user"."deleted_at" IS NULL AND "user"."id" = $2`)).
		WithArgs(sqlmock.AnyArg(), id).
		WillReturnResult(sqlmock.NewResult(1, 1))
	s.mock.ExpectCommit()

	res, err := s.repository.Delete(uuid)

	require.True(s.T(), res)
	require.NoError(s.T(), err)
}

func (s *Suite) TestUserRepository_Delete_Fail() {
	var (
		uuid = "test-uuid"
	)

	// query get user by uuid
	s.mock.ExpectQuery(regexp.QuoteMeta(
		`SELECT * FROM "user" WHERE "user"."deleted_at" IS NULL AND ((uuid = $1))`)).
		WithArgs(uuid).
		WillReturnRows(sqlmock.NewRows([]string{
			"id", "uuid", "username", "password", "salt", "created_at", "updated_at", "deleted_at",
		}))

	res, err := s.repository.Delete(uuid)

	require.False(s.T(), res)
	require.Equal(s.T(), err, errors.New("user"+util.NotFound))
}
