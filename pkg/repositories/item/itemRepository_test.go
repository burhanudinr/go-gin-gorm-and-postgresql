package item

import (
	"database/sql"
	"errors"
	"github.com/DATA-DOG/go-sqlmock"
	model "github.com/burhanudinr/stock-app/pkg/models/item"
	"github.com/burhanudinr/stock-app/pkg/util"
	"github.com/go-test/deep"
	"github.com/jinzhu/gorm"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	"regexp"
	"testing"
	"time"
)

type Suite struct {
	suite.Suite
	DB   *gorm.DB
	mock sqlmock.Sqlmock

	repository Repository
	Item       *model.Item
}

func (s *Suite) SetupSuite() {
	var (
		db  *sql.DB
		err error
	)

	db, s.mock, err = sqlmock.New()
	require.NoError(s.T(), err)

	s.DB, err = gorm.Open("postgres", db)
	require.NoError(s.T(), err)

	s.DB.LogMode(true)

	s.repository = SetRepository(s.DB)
}

func (s *Suite) AfterTest(_, _ string) {
	require.NoError(s.T(), s.mock.ExpectationsWereMet())
}

func TestInit(t *testing.T) {
	suite.Run(t, new(Suite))
}

func (s *Suite) TestItemRepository_GetByUuid() {
	var (
		id      = uint(1)
		uuid    = "test-uuid"
		code    = "test-code"
		name    = "test-name"
		timeNow = time.Now()
	)

	s.mock.ExpectQuery(regexp.QuoteMeta(
		`SELECT * FROM "item" WHERE "item"."deleted_at" IS NULL AND ((uuid = $1))`)).
		WithArgs(uuid).
		WillReturnRows(sqlmock.NewRows([]string{"id", "uuid", "code", "name", "created_at", "updated_at", "deleted_at"}).
			AddRow(id, uuid, code, name, timeNow, timeNow, nil))

	res, err := s.repository.GetByUuid(uuid)

	require.NoError(s.T(), err)
	require.Nil(s.T(), deep.Equal(model.Item{
		Model: gorm.Model{
			ID:        id,
			CreatedAt: timeNow,
			UpdatedAt: timeNow,
			DeletedAt: nil,
		},
		Uuid: uuid,
		Code: code,
		Name: name,
	}, res))
}

func (s *Suite) TestItemRepository_GetAll() {
	var (
		id      = uint(1)
		uuid    = "test-uuid"
		code    = "test-code"
		name    = "test-name"
		timeNow = time.Now()
	)

	s.mock.ExpectQuery(regexp.QuoteMeta(
		`SELECT * FROM "item" WHERE "item"."deleted_at" IS NULL`)).
		WithArgs().
		WillReturnRows(sqlmock.NewRows([]string{"id", "uuid", "code", "name", "created_at", "updated_at", "deleted_at"}).
			AddRow(id, uuid, code, name, timeNow, timeNow, nil).
			AddRow(id, uuid, code, name, timeNow, timeNow, nil).
			AddRow(id, uuid, code, name, timeNow, timeNow, nil))

	res, err := s.repository.GetAll()

	require.NoError(s.T(), err)
	require.Nil(s.T(), deep.Equal(model.Item{
		Model: gorm.Model{
			ID:        id,
			CreatedAt: timeNow,
			UpdatedAt: timeNow,
			DeletedAt: nil,
		},
		Uuid: uuid,
		Code: code,
		Name: name,
	}, res[2]))
}

func (s *Suite) TestItemRepository_Insert() {
	var (
		id   = uint(1)
		code = "test-code"
		name = "test-name"
	)

	s.mock.ExpectBegin()
	s.mock.ExpectQuery(regexp.QuoteMeta(
		`INSERT INTO "item" ("created_at","updated_at","deleted_at","uuid","code","name") VALUES ($1,$2,$3,$4,$5,$6) RETURNING "item"."id"`)).
		WithArgs(sqlmock.AnyArg(), sqlmock.AnyArg(), nil, sqlmock.AnyArg(), code, name).
		WillReturnRows(
			sqlmock.NewRows([]string{"id"}).
				AddRow(id))
	s.mock.ExpectCommit()

	res, err := s.repository.Insert(model.Item{
		Code: code,
		Name: name,
	})

	require.NoError(s.T(), err)
	require.Nil(s.T(), deep.Equal(model.Item{
		Model: gorm.Model{
			ID:        id,
			CreatedAt: res.CreatedAt,
			UpdatedAt: res.UpdatedAt,
			DeletedAt: nil,
		},
		Uuid: res.Uuid,
		Code: code,
		Name: name,
	}, res))
}

func (s *Suite) TestItemRepository_InsertWithUuid() {
	var (
		id   = uint(1)
		uuid = "test-uuid"
		code = "test-code"
		name = "test-name"
	)

	s.mock.ExpectBegin()
	s.mock.ExpectQuery(regexp.QuoteMeta(
		`INSERT INTO "item" ("created_at","updated_at","deleted_at","uuid","code","name") VALUES ($1,$2,$3,$4,$5,$6) RETURNING "item"."id"`)).
		WithArgs(sqlmock.AnyArg(), sqlmock.AnyArg(), nil, uuid, code, name).
		WillReturnRows(
			sqlmock.NewRows([]string{"id"}).
				AddRow(id))
	s.mock.ExpectCommit()

	res, err := s.repository.Insert(model.Item{
		Uuid: uuid,
		Code: code,
		Name: name,
	})

	require.NoError(s.T(), err)
	require.Nil(s.T(), deep.Equal(model.Item{
		Model: gorm.Model{
			ID:        id,
			CreatedAt: res.CreatedAt,
			UpdatedAt: res.UpdatedAt,
			DeletedAt: nil,
		},
		Uuid: uuid,
		Code: code,
		Name: name,
	}, res))
}

func (s *Suite) TestItemRepository_Update() {
	var (
		id      = uint(1)
		uuid    = "test-uuid"
		code    = "test-code"
		name    = "test-name"
		timeNow = time.Now()
	)

	// query get uuid exist
	s.mock.ExpectQuery(regexp.QuoteMeta(
		`SELECT * FROM "item" WHERE "item"."deleted_at" IS NULL AND ((uuid = $1))`)).
		WithArgs(uuid).
		WillReturnRows(sqlmock.NewRows([]string{"id", "uuid", "code", "name", "created_at", "updated_at", "deleted_at"}).
			AddRow(id, uuid, code, name, timeNow, timeNow, nil))

	// query update
	s.mock.ExpectBegin()
	s.mock.ExpectExec(regexp.QuoteMeta(
		`UPDATE "item" SET "code" = $1, "id" = $2, "name" = $3, "updated_at" = $4, "uuid" = $5
			WHERE "item"."deleted_at" IS NULL AND "item"."id" = $6`)).
		WithArgs(code, id, name, sqlmock.AnyArg(), uuid, id).
		WillReturnResult(sqlmock.NewResult(1, 1))
	s.mock.ExpectCommit()

	res, err := s.repository.Update(model.Item{
		Uuid: uuid,
		Code: code,
		Name: name,
	})

	require.NoError(s.T(), err)
	require.Nil(s.T(), deep.Equal(model.Item{
		Model: gorm.Model{
			ID:        id,
			CreatedAt: time.Time{},
			UpdatedAt: time.Time{},
			DeletedAt: nil,
		},
		Uuid: uuid,
		Code: code,
		Name: name,
	}, res))
}

func (s *Suite) TestItemRepository_Update_Fail() {
	var (
		uuid = "test-uuid"
		code = "test-code"
		name = "test-name"
	)

	// query get uuid exist
	s.mock.ExpectQuery(regexp.QuoteMeta(
		`SELECT * FROM "item" WHERE "item"."deleted_at" IS NULL AND ((uuid = $1))`)).
		WithArgs(uuid).
		WillReturnRows(sqlmock.NewRows([]string{
			"id", "uuid", "code", "name", "created_at", "updated_at", "deleted_at",
		}))

	res, err := s.repository.Update(model.Item{
		Uuid: uuid,
		Code: code,
		Name: name,
	})

	require.Equal(s.T(), err, errors.New("item"+util.NotFound))
	require.Equal(s.T(), model.Item{}, res)
}

func (s *Suite) TestItemRepository_Delete() {
	var (
		id      = uint(1)
		uuid    = "test-uuid"
		code    = "test-code"
		name    = "test-name"
		timeNow = time.Now()
	)

	// query get item by uuid
	s.mock.ExpectQuery(regexp.QuoteMeta(
		`SELECT * FROM "item" WHERE "item"."deleted_at" IS NULL AND ((uuid = $1))`)).
		WithArgs(uuid).
		WillReturnRows(sqlmock.NewRows([]string{"id", "uuid", "code", "name", "created_at", "updated_at", "deleted_at"}).
			AddRow(id, uuid, code, name, timeNow, timeNow, nil))

	// query delete
	s.mock.ExpectBegin()
	s.mock.ExpectExec(regexp.QuoteMeta(
		`UPDATE "item" SET "deleted_at"=$1 WHERE "item"."deleted_at" IS NULL AND "item"."id" = $2`)).
		WithArgs(sqlmock.AnyArg(), id).
		WillReturnResult(sqlmock.NewResult(1, 1))
	s.mock.ExpectCommit()

	res, err := s.repository.Delete(uuid)

	require.True(s.T(), res)
	require.NoError(s.T(), err)
}

func (s *Suite) TestItemRepository_Delete_Fail() {
	var (
		uuid = "test-uuid"
	)

	// query get item by uuid
	s.mock.ExpectQuery(regexp.QuoteMeta(
		`SELECT * FROM "item" WHERE "item"."deleted_at" IS NULL AND ((uuid = $1))`)).
		WithArgs(uuid).
		WillReturnRows(sqlmock.NewRows([]string{
			"id", "uuid", "code", "name", "created_at", "updated_at", "deleted_at",
		}))

	res, err := s.repository.Delete(uuid)

	require.False(s.T(), res)
	require.Equal(s.T(), err, errors.New("item"+util.NotFound))
}
