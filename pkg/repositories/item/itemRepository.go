package item

import (
	"errors"
	model "github.com/burhanudinr/stock-app/pkg/models/item"
	"github.com/burhanudinr/stock-app/pkg/util"
	"github.com/google/uuid"
	"github.com/jinzhu/gorm"
)

type repo struct {
	db *gorm.DB
}

type Repository interface {
	GetAll() ([]model.Item, error)
	Insert(item model.Item) (model.Item, error)
	GetByUuid(uuid string) (model.Item, error)
	Update(item model.Item) (model.Item, error)
	Delete(uuid string) (bool, error)
}

func SetRepository(db *gorm.DB) Repository {
	return &repo{
		db: db,
	}
}

func (p *repo) GetAll() ([]model.Item, error) {
	var items []model.Item
	err := p.db.Find(&items).Error

	return items, err
}

func (p *repo) Insert(item model.Item) (model.Item, error) {
	if item.Uuid == "" {
		item.Uuid = uuid.New().String()
	}
	p.db.Create(&item)
	return item, nil
}

func (p *repo) GetByUuid(uuid string) (model.Item, error) {
	item := model.Item{}
	err := p.db.Where("uuid = ?", uuid).Find(&item).Error

	return item, err
}

func (p *repo) Update(item model.Item) (model.Item, error) {
	// get item by uuid
	var itemExist model.Item
	if p.db.Where("uuid = ?", item.Uuid).Find(&itemExist).RecordNotFound() {
		return model.Item{}, errors.New(p.db.NewScope(&itemExist).TableName() + util.NotFound)
	}

	item.ID = itemExist.ID
	p.db.Model(&itemExist).Update(item)

	return item, nil
}

func (p *repo) Delete(uuid string) (bool, error) {
	// get item by uuid
	var item model.Item
	if p.db.Where("uuid = ?", uuid).Find(&item).RecordNotFound() {
		return false, errors.New(p.db.NewScope(&item).TableName() + util.NotFound)
	}

	p.db.Model(&item).Delete(item)

	return true, nil
}
